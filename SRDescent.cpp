#include "SRDescent.h"
#include <algorithm>
#include <cstdarg>
#include <fstream>
#include <string>

#if defined( __GNUC__ )
#	include <cxxabi.h>
#	include <cstdlib>
#endif

#if defined( WIN32 ) || defined( _WIN32 )
#	include <windows.h>
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#else
#	include <dlfcn.h>
__attribute__( ( visibility( "default" ) ) ) [[maybe_unused]] void *SRDescent_ImageBase;
#endif

namespace {
	std::string getLogFileName() {
#if defined( WIN32 ) || defined( _WIN32 )
		std::string module_path;
		module_path.resize( MAX_PATH, '\0' );
		GetModuleFileNameA( (HMODULE)&__ImageBase, (char *)module_path.data(), MAX_PATH );
		auto sep = module_path.rfind( "\\" );
		if ( sep == std::string::npos ) sep = module_path.rfind( "/" );
		auto module = module_path.substr( sep + 1 );
		return "!SRDescent_" + module + ".log";
#else
		Dl_info info;
		dladdr( (void *)&SRDescent_ImageBase, &info );
		std::string module_path = info.dli_fname;
		auto		sep			= module_path.rfind( "/" );
		auto		module		= module_path.substr( sep + 1 );
		return "!SRDescent_" + module + ".log";
#endif
	}

	std::ofstream &getLogStream() {
		static std::ofstream out;
		if ( !out.is_open() ) {
			auto		  fname = getLogFileName();
			std::ifstream check( fname );
			if ( !check.good() ) return out; // file not exist
			out.open( fname );
		}
		return out;
	}

	template<class C> std::string getClassName( C *obj ) {
#if defined( __GNUC__ )
		int			status;
		auto		pClassName = abi::__cxa_demangle( typeid( *obj ).name(), 0, 0, &status );
		std::string result	   = pClassName;
		if ( status != 0 && !pClassName )
			result = typeid( *obj ).name();
		else
			free( pClassName );
		return result;
#else
		return typeid( *obj ).name();
#endif
	}

	void log( const char *fmt, ... ) {
		static std::mutex mtx;
		static char		  static_buffer[4096]{ 0 };

		std::lock_guard lock( mtx ); // use mutex instead of stringstream

		if ( !getLogStream().is_open() ) return; // can't open file

		if ( fmt == nullptr ) {
			getLogStream().flush();
			return;
		}

		va_list argptr;
		va_start( argptr, fmt );

		vsprintf( static_buffer, fmt, argptr );
		va_end( argptr );

		getLogStream() << static_buffer << std::endl;
		getLogStream().flush();
	}
} // namespace

SRDescent::SRDescent( SRDescent *parent ) {
	setParent( parent );
}

SRDescent::~SRDescent() {
	if ( getThreadId() != std::this_thread::get_id() ) _mtx.lock();
	for ( auto &&child : _childs ) {
		if ( child != nullptr ) {
			delete child;
			child = nullptr;
		}
	}
	_childs.clear();
	if ( getThreadId() != std::this_thread::get_id() ) _mtx.unlock();
}

const SRDescent *SRDescent::parent() const {
	return _parent;
}

void SRDescent::setParent( SRDescent *parent ) {
	if ( getThreadId() != std::this_thread::get_id() ) _mtx.lock();
	if ( _parent != nullptr ) _parent->removeChild( this );

	_parent = parent;
	if ( _parent != nullptr ) _parent->addChild( this );
	if ( getThreadId() != std::this_thread::get_id() ) _mtx.unlock();
}

std::thread::id SRDescent::getThreadId() const {
	return _tid;
}

void SRDescent::moveToThread( std::thread::id tid ) {
	std::lock_guard lock( _mtx );
	_tid = tid;
}

void SRDescent::addChild( SRDescent *child ) {
	if ( child == nullptr ) return;
	if ( getThreadId() != std::this_thread::get_id() ) _mtx.lock();
	if ( getThreadId() != child->getThreadId() ) {
		auto parent_name = ::getClassName( this );
		auto child_name	 = ::getClassName( child );
		log( "Different threads of parent (%s[%X] tid %d) and child (%s[%X] tid %d)", parent_name.c_str(), this, getThreadId(),
			   child_name.c_str(), child, child->getThreadId() );
	}
	_childs.push_back( child );
	if ( getThreadId() != std::this_thread::get_id() ) _mtx.unlock();
}

bool SRDescent::removeChild( SRDescent *child ) {
	if ( getThreadId() != std::this_thread::get_id() ) _mtx.lock();
	auto it = std::find( _childs.begin(), _childs.end(), child );
	if ( it == _childs.end() ) {
		if ( getThreadId() != std::this_thread::get_id() ) _mtx.unlock();
		return false;
	}
	_childs.erase( it );
	if ( getThreadId() != std::this_thread::get_id() ) _mtx.unlock();
	return true;
}
