#ifndef SRDESCENT_H
#define SRDESCENT_H

#include <deque>
#include <thread>
#include <mutex>

/// Класс позволяет указать родительский класс, что бы автоматически удалять дочерние классы.
class SRDescent {
public:
	SRDescent() = delete;
	/// \param[in] parent Родитель этого класса
	explicit SRDescent( SRDescent *parent = nullptr );
	virtual ~SRDescent();

	/// \return Указатель на родителя.
	[[nodiscard]] const SRDescent *parent() const;
	/**
	 * \brief Устанавливает родителя (смена родителя)
	 * \param[in] parent родитель
	 */
	void setParent( SRDescent *parent );

	/// \return id потока в котором был сконструерован класс
	[[nodiscard]] std::thread::id getThreadId() const;

	/**
	 * \brief Меняет id потока владеющего объектом
	 * \param tid новый поток-владелец
	 */
	void moveToThread( std::thread::id tid );

protected:
	/**
	 * \brief Добавляет дочерний объект
	 * \param[in] child дочерний объект
	 */
	void addChild( SRDescent *child );
	/**
	 * \brief Удаляет дочерний объект
	 * \param[in] child дочерний объект
	 * \return \b bool -> успешность выполнения
	 */
	bool removeChild( SRDescent *child );

private:
	SRDescent *				_parent = nullptr;
	std::deque<SRDescent *> _childs;
	std::thread::id			_tid = std::this_thread::get_id();
	std::mutex				_mtx;
};

#endif // SRDESCENT_H
